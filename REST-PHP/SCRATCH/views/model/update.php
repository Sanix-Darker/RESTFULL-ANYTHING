<?php 

	$result = array();
	$output = array();
	if($required){

		// Insertion
		$BD->from($element)
		    ->where($where)
		    ->update($data)
		    ->execute();

		$rows = $BD->from($element)
			->where($where)
			->select()
			->one();

		foreach ($rows as $key => $val) {
		  if (!is_int($key)) {
		    $output[$key]=$rows[$key];
		  }
		}
		$result['status'] = 'success';
		$result['result'] = $output;
		
	}else{
		$result['status'] = 'error';
		$result['reason'] = "Please, check your entries";	
	}

	echo json_encode($result);
?>